<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=US-ASCII" pageEncoding="US-ASCII"%>
<!DOCTYPE html>

<sql:query var="rs" dataSource="jdbc/phonebook">
select id, firstName from contact
</sql:query>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
	<title>PhoneBook</title>
	<link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="css/styles.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
	<script
	  src="https://code.jquery.com/jquery-3.2.1.min.js"
	  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
	  crossorigin="anonymous"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
	  
	<script type="text/javascript" src="js/phonebook.js"></script>
</head>
<body>

					    
	<div ng-app="myApp" ng-controller="customersCtrl"> 
		<div class="container">
			<div class="row" style="padding-bottom: 20px;">
				<div class="pure-u-1">
					<div class="header">
						<img class="logo" src="img/phonebook.png"/>
						<p>v 1.0</p>
					</div>
					
				</div>
			</div>
			<div class="row" style="padding-top: 20px;">
			    <div class="col">
			    	<div class="box">
			    		<h2><i class="fa fa-user-plus"></i>New contact</h2>
			    		<form id="addContactForm" class="pure-form" ng-submit="addContactNg()" >
						    <fieldset class="pure-group">
						        <input type="text" name="firstName" class="pure-input-1-2" placeholder="First Name">
						        <input type="text" name="lastName" class="pure-input-1-2" placeholder="Last Name">
						        <input type="text" name="phone" class="pure-input-1-2" placeholder="Phone">
						    </fieldset>
						    <button type="submit" class="pure-button pure-input-1-2 pure-button-primary">
						    <i class="fa fa-user-plus"></i>Add</button>
						</form>
					</div>
				</div>
			    <div class="col">
					<div class="box">
			    		<h2><i class="fa fa-search"></i>Search contact</h2>
			    		<form id="findContactForm" class="pure-form" ng-submit="findContactNg()" >
			    			<fieldset class="pure-group">
						    	<input type="text" ng-model="word" name="word" class="pure-input-1-2">
						     </fieldset>
						    <button type="submit" class="pure-button pure-input-1-2 pure-button-primary">
						    <i class="fa fa-search"></i>Search</button>
						</form>
					</div>
				</div>
				<div class="col">
					<div class="box">
			    		<h2><i class="fa fa-users"></i> Contacts</h2>
		    			<table class="pure-table">
						    <thead>
						        <tr>
						            <th>First Name</th>
						            <th>Last Name</th>
						            <th>Phone</th>
						        </tr>
						    </thead>
						
						    <tbody>
		    					
							        <tr ng-repeat="contact in contacts">
							            <td>{{contact.firstName}}</td>
							            <td>{{contact.lastName}}</td>
							            <td>{{contact.phone}}</td>
							        </tr>
		    					
	    					
						        
						    </tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>