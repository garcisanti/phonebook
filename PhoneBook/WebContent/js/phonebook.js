var app = angular.module('myApp', []);
app.controller('customersCtrl', [ '$scope', '$http', function($scope, $http) {
	$scope.word = '';
    $http.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded; charset=utf-8";
    $http({
        url : '/PhoneBook/getContacts',
        method : "GET",
        params : {
            'word' : $scope.word
        }
    }).then(function(response) {
        console.log(response.data);
        $scope.contacts = response.data;
    }, function(response) {
        //fail case
        console.log(response);
    });
    
    $scope.addContactNg = function() {
        $http({
            url : '/PhoneBook/addContact',
            method : "POST",
            data:$('#addContactForm').serialize(),
        }).then(function(response) {
            console.log(response.data);
            $scope.contacts = response.data;
        }, function(response) {
            //fail case
            console.log(response);
        });
 
    };
     
    $scope.findContactNg = function() {
        $http({
            url : '/PhoneBook/findContact',
            method : "POST",
            params : {
                'word' : $scope.word
            }
        }).then(function(response) {
            console.log(response.data);
            $scope.contacts = response.data;
        }, function(response) {
            //fail case
            console.log(response);
        });
 
    };
} ]);

/* Phonebook */
