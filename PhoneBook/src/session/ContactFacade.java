package session;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import entity.Contact;

public class ContactFacade 
{

	private EntityManager em;
	
	protected void setEntityManager() {
		EntityManagerFactory emf =
	    Persistence.createEntityManagerFactory("ContactPhonePU");
		em = emf.createEntityManager();
	}

	public List<Contact> findAll() {
		setEntityManager();
		javax.persistence.criteria.CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
		cq.select(cq.from(Contact.class));
		List<Contact> l = em.createQuery(cq).getResultList();
		em.close();
		return l;
	}
	
	public void create(Contact entity) {
		setEntityManager();
		em.getTransaction().begin();
        em.persist(entity);
        em.getTransaction().commit();
        em.close();
    }
	
	public List<Contact> find(String word) {
		setEntityManager();		
		List<Contact> l = em.createQuery(
		        "SELECT c FROM Contact c WHERE (c.firstName LIKE :custName OR c.lastName LIKE :custName OR c.phone LIKE :custName)")
		        .setParameter("custName", "%"+word+"%")
		        .getResultList();
		em.close();
		return l;
    }
	
	

}
