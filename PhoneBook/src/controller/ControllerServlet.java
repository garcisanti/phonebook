package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import entity.Contact;
import session.ContactFacade;


/**
 * Servlet implementation class ControllerServlet
 */
@WebServlet(name = "ControllerServlet",
loadOnStartup = 1,
urlPatterns = {
               "/addContact",
               "/findContact",
               "/getContacts"})
public class ControllerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
//    public ControllerServlet() {
//        super();
//        // TODO Auto-generated constructor stub
//    }
    
//    @EJB
//    private ContactFacade contactFacade;

    public void init() throws ServletException {
    	
//    	 EntityManagerFactory emf =
//    	           (EntityManagerFactory)getServletContext().getAttribute("emf");
//    	        EntityManager em = emf.createEntityManager();
//    	 
//    	        try {
//    	            
//    	 
//    	            // Display the list of guests:
//    	            List<Contact> contactList = em.createQuery(
//    	                "SELECT c FROM Contact c", Contact.class).getResultList();
//    	            getServletContext().setAttribute("contacts", contactList);
//    	 
//    	        } finally {
//    	            // Close the database connection:
//    	            if (em.getTransaction().isActive())
//    	                em.getTransaction().rollback();
//    	            em.close();
//    	        }
        // store category list in servlet context
    	Context initCtx = null;
		try {
			initCtx = new InitialContext();
	    	Context envCtx = (Context) initCtx.lookup("java:comp/env");
	    	ContactFacade contactFacade = (ContactFacade) envCtx.lookup("bean/ContactFacade");
	        getServletContext().setAttribute("contacts", contactFacade.findAll());
	        
	    	
		} 
		catch (NamingException e) {
			e.printStackTrace();
		}
		finally {    
			  try { 
				  initCtx.close(); 
			  } catch (Exception e) {
					e.printStackTrace();
					}
		}
    	
    	
    	System.out.println("Servlet Init");
//        getServletContext().setAttribute("contacts", contactFacade.findAll());
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		String userPath = request.getServletPath();
//    	EntityManagerFactory emf =
// 	           (EntityManagerFactory)getServletContext().getAttribute("emf");
//     EntityManager em = emf.createEntityManager();
//		response.getWriter().append("Served at: ").append(request.getContextPath());
//		Context initCtx = null;
//		try {
//			initCtx = new InitialContext();
//	    	Context envCtx = (Context) initCtx.lookup("java:comp/env");
//	    	ContactFacade contactFacade = (ContactFacade) envCtx.lookup("bean/ContactFacade");
//	        getServletContext().setAttribute("contacts", contactFacade.findAll());
//	        
//	    	
//		} 
//		catch (NamingException e) {
//			e.printStackTrace();
//		}
//		finally {    
//			  try { 
//				  initCtx.close(); 
//			  } catch (Exception e) {
//					e.printStackTrace();
//					}
//		}
//    	System.out.println("Servlet GET");

		if (userPath.equals("/getContacts")) {
			Context initCtx = null;
			try {
				initCtx = new InitialContext();
		    	Context envCtx = (Context) initCtx.lookup("java:comp/env");
		    	ContactFacade contactFacade = (ContactFacade) envCtx.lookup("bean/ContactFacade");
		    	List<Contact> l = contactFacade.findAll();
		        getServletContext().setAttribute("contacts", l);
		        String json = new Gson().toJson(l);
		        System.out.println(json);			        

		        response.setContentType("application/json");
		        response.setCharacterEncoding("UTF-8");
		        response.getWriter().write(json);
		    	
			} 
			catch (NamingException e) {
				e.printStackTrace();
			}
			finally {    
				  try { 
					  initCtx.close(); 
				  } catch (Exception e) {
						e.printStackTrace();
						}
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String userPath = request.getServletPath();
		System.out.println(userPath);
		if (userPath.equals("/addContact")) {
			// TODO Auto-generated method stub
			String firstName = request.getParameter("firstName");
			String lastName = request.getParameter("lastName");
			String phone = request.getParameter("phone");
			
			// Compruebo que los campos del formulario tienen datos para a�adir a la tabla
			if (!firstName.equals("") && !lastName.equals("") && !phone.equals("")) {
				// Creo el objeto persona y lo a�ado al arrayList
				Contact contact = new Contact(3, firstName, lastName, phone);
				Context initCtx = null;
				try {
					initCtx = new InitialContext();
			    	Context envCtx = (Context) initCtx.lookup("java:comp/env");
			    	ContactFacade contactFacade = (ContactFacade) envCtx.lookup("bean/ContactFacade");
			    	contactFacade.create(contact);
			    	List<Contact> l = contactFacade.findAll();
			        getServletContext().setAttribute("contacts", l);
			        String json = new Gson().toJson(l);
			        System.out.println(json);			        

			        response.setContentType("application/json");
			        response.setCharacterEncoding("UTF-8");
			        response.getWriter().write(json);			        
				} 
				catch (NamingException e) {
					e.printStackTrace();
				}
				finally {    
					  try { 
						  initCtx.close(); 
					  } catch (Exception e) {
							e.printStackTrace();
							}
				}
			}
		}
		else if (userPath.equals("/findContact")) {

			String word = request.getParameter("word");
			if(!word.equals("")) {
				Context initCtx = null;
				try {
					initCtx = new InitialContext();
			    	Context envCtx = (Context) initCtx.lookup("java:comp/env");
			    	ContactFacade contactFacade = (ContactFacade) envCtx.lookup("bean/ContactFacade");
			    	List<Contact> l = contactFacade.find(word);
			        getServletContext().setAttribute("contacts", l);
			        String json = new Gson().toJson(l);
			        System.out.println(json);			        

			        response.setContentType("application/json");
			        response.setCharacterEncoding("UTF-8");
			        response.getWriter().write(json);		        
			    	
				} 
				catch (NamingException e) {
					e.printStackTrace();
				}
				finally {    
					  try { 
						  initCtx.close(); 
					  } catch (Exception e) {
							e.printStackTrace();
							}
				}
			}
			else {
				Context initCtx = null;
				try {
					initCtx = new InitialContext();
			    	Context envCtx = (Context) initCtx.lookup("java:comp/env");
			    	ContactFacade contactFacade = (ContactFacade) envCtx.lookup("bean/ContactFacade");
			    	List<Contact> l = contactFacade.findAll();
			        getServletContext().setAttribute("contacts", l);
			        String json = new Gson().toJson(l);
			        System.out.println(json);			        

			        response.setContentType("application/json");
			        response.setCharacterEncoding("UTF-8");
			        response.getWriter().write(json);
			    	
				} 
				catch (NamingException e) {
					e.printStackTrace();
				}
				finally {    
					  try { 
						  initCtx.close(); 
					  } catch (Exception e) {
							e.printStackTrace();
							}
				}
			}
		}
	}
	

}
